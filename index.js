require('dotenv').config()
const namor = require("namor")
const bcrypt = require("bcryptjs")

const express = require('express')
const app = express()

app.use(express.json());
app.use(express.urlencoded());

const { Voter } = require('./model');

app.get("/", async (request, reply) => {
  reply.send("pong ping pong")
})

// Declare a route
app.post('/', async (request, reply) => {
  if (!request.body || !request.body.password || !request.body.nic || !request.body.phoneNumber) {
    reply.statusCode = 400
    return reply.send({ message: "Parameters missing" })
  }
  const { password, nic, phoneNumber } = request.body
  // ============= Validations/Verifications Start============
  // Example : ensure password is strong
  // Verify if the phone number is  registered under the nic by accessing external APIs
  // If verifcation/validations aren't satisfactory throw an error

  const existingVoter = await Voter.findOne({ nic }).exec();
  if (existingVoter) {
    reply.statusCode = 403
    return reply.send({ message: "Voter already registered" })
  }
  // ============= Validations/Verifications End============


  // Assume name comes from nic service after validation
  const nameFromNicService = namor.generate({
    words: 2,
    separator: " ",
    saltLength: 0
  })



  const hashedPw = await bcrypt.hash(password, 12);

  const voterData = {
    name: nameFromNicService,
    password: hashedPw,
    nic: nic,
    phoneNumber: phoneNumber,
    // Some logic should be done during validation/ verification process to know if this person is eligible or not
    status: "eligible" 
  }
  const voter = new Voter(voterData);


  await voter.save()
  console.log('Registered successfully');
  reply.statusCode = 201
  return reply.send({ message: "Registered successfully" })
})

const port = 3000;
// Run the server!
app.listen(port, function () {
  console.log(`Server running on port : ${port}`)
  // Server is now listening on ${address}
})
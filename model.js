const mongoose = require('mongoose');
try {
  mongoose.set('strictQuery', false);
  mongoose.connect(process.env.MONGO_URI);
} catch (error) {
  console.log(MONGO_URI)
  console.log("Error during mongo connection", error)
}

const Voter = mongoose.model('Voter', {
  name: String,
  password: String,
  nic: String,
  phoneNumber: String,
  status: String,
});



module.exports = { Voter }